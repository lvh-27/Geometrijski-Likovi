package application;

public class Pravokutnik extends Kvadrat{
	
	private double b;

	public Pravokutnik(double a, double b) {
		super(a);
		this.setB(b);
	}

	public double getB() {
		return b;
	}

	public void setB(double b) {
		this.b = b;
	}
	
	@Override	// je li potrebno?
	public double povrsina() {
		double pov = this.getA() * this.b;
		return pov;
	}
	
	@Override
	public double opseg() {
		double op = (this.getA() + this.b) * 2;
		return op;
	}

	public static void main(String[] args) {
		Pravokutnik p1 = new Pravokutnik(5, 7);
		System.out.println("P: " + p1.povrsina());
		System.out.println("O: " + p1.opseg());
		System.out.println("a = " + p1.getA());

	}
	
}
