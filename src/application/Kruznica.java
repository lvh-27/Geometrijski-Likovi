package application;

public class Kruznica {
	
	private double r;
	
	public Kruznica(double r) {
		this.r = r;
	}

	public double getR() {
		return r;
	}

	public void setR(double r) {
		this.r = r;
	}
	
	public double povrsina() {
		double pov = Math.pow(this.r, 2) * Math.PI;
		return pov;
	}
	
	public double opseg() {
		double op = 2 * this.r * Math.PI;
		return op;
	}
	
	public static void main(String[] args) {
		Kruznica k1 = new Kruznica(5);
		System.out.println("P: " + k1.povrsina());
		System.out.println("O: " + k1.opseg());
	}
	
}
