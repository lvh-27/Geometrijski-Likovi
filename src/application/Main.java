package application;
	
import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception{
		
		}
	
	public static void main(String[] args) {
		System.out.println("Trokut:");
		Trokut.main(args);
		System.out.println("\nKvadrat:");
		Kvadrat.main(args);
		System.out.println("\nPravokutnik:");
		Pravokutnik.main(args);
		System.out.println("\nKružnica:");
		Kruznica.main(args);
	}

}
