package application;

public class Trokut {

	private double a;
	private double b;
	private double c;
	private double s;
	
	public Trokut(double a, double b, double c) {
		this.a = a;
		this.b = b;
		this.c = c;
	}
	
	public double getA() {
		return a;
	}

	public void setA(double a) {
		this.a = a;
	}

	public double getB() {
		return b;
	}

	public void setB(double b) {
		this.b = b;
	}

	public double getC() {
		return c;
	}

	public void setC(double c) {
		this.c = c;
	}

	public double getS() {
		s = (a + b + c) / 2;
		return s;
	}

	public double povrsina() {
		s = this.getS();
		double pov = Math.sqrt(s*(s-this.a)*(s-this.b)*(s-this.c)); 	// ne znam treba li "this."
		return pov;
	}
	
	public double opseg() {
		double op = this.a + this.b + this.c;		// ne znam treba li "this."
		return op;		
	}
	
	public static void main(String[] args) {
		Trokut t1 = new Trokut(5, 7, 9);
		Trokut t2 = new Trokut(2, 3, 4);
		
		System.out.println("s1 = " + t1.getS());
		System.out.println("s2 = " + t2.getS());
		System.out.println("P1: " + t1.povrsina());
		System.out.println("P2: " + t2.povrsina());
		System.out.println("O1: " + t1.opseg());
		t1.setA(10);
		System.out.println("P1: " + t1.povrsina());
		System.out.println("O1: " + t1.opseg());
	}
}
