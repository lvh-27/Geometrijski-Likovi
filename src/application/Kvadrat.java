package application;

public class Kvadrat {

	private double a;

	public Kvadrat(double a) {
		this.a = a;
	}
	
	public double getA() {
		return a;
	}

	public void setA(double a) {
		this.a = a;
	}
	
	public double povrsina() {
		double pov = Math.pow(this.a, 2);
		return pov;
	}
	
	public double opseg() {
		double op = 4*this.a;
		return op;
	}
	
	public static void main(String[] args) {
		Kvadrat k1 = new Kvadrat(5);
		System.out.println("O: " + k1.opseg());
		System.out.println("P: " + k1.povrsina());
		k1.setA(k1.getA() + 2);
		System.out.println("a = " + k1.getA());
		System.out.println("O: " + k1.opseg());
		System.out.println("P: " + k1.povrsina());
	}
	
}
